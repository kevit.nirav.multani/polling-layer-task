const rp = require('request-promise')

const pollRequest = () => {
    return new Promise((resolve, reject) => {
        var options = {
            //uri: 'http://localhost:3000/getRandNo',
            uri: `${process.env.URL}getRandNo`,
            json: true
        }

        rp(options)
            .then((response) => {
                // console.log(response)
                resolve(response)               
            })
            .catch(function (err) {
                console.error(err)
                reject(err)
            });
    })
}

module.exports = pollRequest
