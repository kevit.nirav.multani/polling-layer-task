### Usage:

`npm install` 

`npm run start`

### Definition:

There are 2 routes.

##### Route 1:-

- Generated random number between 0-15

- if number is greter then 10 then it should return **not success**.

- else return (increment variable) on success.

##### Route 2:-

- Call **route 1** until it returns success.

- After that gives response that is given by **route 1**

`Goal: Convert unpredictive service into predictive service by adding layer of polling.`

### Notes:

- copy all variable from example.env to .env in root 

- keep the variable and value as it is if you're using localhost otherwise change it with ngrok url