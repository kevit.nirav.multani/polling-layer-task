const express = require('express')
const func = require('../utils/requestToGenNo')
const router = new express.Router()

router.get('/', async (req, res) => {

    let result = "failure"
    let response = ""

    while(result === "failure"){
        response = await func()
        result = response.result
    }

    res.send(response)
})

module.exports = router