const express = require('express')
const router = new express.Router()

let min = 0
let max = 15
let counter = 0

function getRandomIntInclusive() {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}

router.get('/getRandNo', (req, res) => {
    const number = getRandomIntInclusive()
    console.log(number)
    if(number > 10){
        return res.send({
            result: "failure"
        })
    }
    res.send({
        result: "success",
        counter
    })
    counter++
})

module.exports = router
