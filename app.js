const express = require('express')
const app = express()
const noRouter = require('./router/generateRandNo')
const pollRouter = require('./router/pollingLayer')

app.use(noRouter)
app.use(pollRouter)

app.listen(process.env.PORT, () => {
    console.log('server started on port ' + process.env.PORT)
})